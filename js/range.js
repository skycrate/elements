// Light Range Slider //
var slider_light = document.getElementById("range_light");
var output_light = document.getElementById("range_out");
output_light.innerHTML = slider_light.value;
slider_light.oninput = function () {
    "use strict";
    output_light.innerHTML = this.value;
};

// Dark Range Slider //
// let slider_dark = document.getElementById("range_dark");
// let output_dark = document.getElementById("range_dark");
// output_dark.innerHTML = slider_dark.value;
// slider_dark.oninput = function() {
//     output_dark.innerHTML = this.value;
// }