// Password Strength Meter Light //
function passwordStrengthLight(password){ 
    var score = 0;
    //if password longer than 3 characters award 1 point //
    if (password.length > 3) score++;
    //if password has both lower and uppercase characters give 1 point //
    if ( ( password.match(/[a-z]/) ) && ( password.match(/[A-Z]/) ) ) score++;
    //if password has at least one number give 1 point //
    if (password.match(/\d+/)) score++;
    //if password has at least one special character give 1 point //
    if ( password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/) ) score++;
    //if password is longer than 8 characters award 1 point //
    if (password.length > 8) score++; 
    document.getElementById("passwordStrengthLight").className = "strength" + score;
}

// Password Strength Meter Dark //
function passwordStrengthDark(password){ 
    var score = 0;
    //if password longer than 3 characters award 1 point //
    if (password.length > 3) score++;
    //if password has both lower and uppercase characters give 1 point //
    if ( ( password.match(/[a-z]/) ) && ( password.match(/[A-Z]/) ) ) score++;
    //if password has at least one number give 1 point //
    if (password.match(/\d+/)) score++;
    //if password has at least one special character give 1 point //
    if ( password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/) ) score++;
    //if password is longer than 8 characters award 1 point //
    if (password.length > 8) score++; 
    document.getElementById("passwordStrengthDark").className = "strength" + score;
}

// Show / Hide Passowrd Light //
var passLight = document.getElementById('pass-light');
var showLight = document.getElementById('show-light');
showLight.addEventListener('click', togglePassLight);

function togglePassLight(){
    showLight.classList.toggle('active');
    (passLight.type == 'password') ? passLight.type ='text' : passLight.type ='password';
}

// Show / Hide Passowrd Dark //
var passDark = document.getElementById('pass-dark');
var showDark = document.getElementById('show-dark');
showDark.addEventListener('click', togglePassDark);

function togglePassDark(){
    showDark.classList.toggle('active');
    (passDark.type == 'password') ? passDark.type ='text' : passDark.type ='password';
}


