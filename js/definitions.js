// @ts-nocheck
const ROWS = Symbol('rows');
const COLS = Symbol('cols');
const CONNECTED = Symbol("connected");
const LISTENERS = Symbol("listeners");
const RECTANGLE = Symbol("rectabgle");
const RESIZE = 'resize';
const AT = '@';

const sqr = a => a * a;
const scale = f => a => a * f;
const add = (a, b) => a + b;
const sub = (a, b) => a - b;
const mul = (a, b) => a * b;
const div = (a, b) => a / b;
const px = a => `${a}px`;
const int = b => parseInt(b);
const set = (obj, name, value, enumerable = false, writable = false) =>
				Object.defineProperty(obj, name, {value, enumerable, writable});
const values = (obj, properties, enumerable = false) => Object.entries(properties)
										.forEach(([key, value]) => set(obj, key, value, enumerable));
const variables = (obj, properties, enumerable = false) => Object.entries(properties)
										.forEach(([key, value]) => set(obj, key, value, enumerable, true));
const vector = (...coords) => new Vector(...coords);
class Vector extends Array {
	get x() {
		return this[0];
	}
	get y() {
		return this[1];
	}
	get z() {
		return this[2];
	}
	constructor(...args) {
		super(...args);
	}
	concat(vector, operator = add) {
		if (vector.length !== this.length)
			throw "ERROR: vectors are not of the same size.";
		return this.map((x, i) => operator(x, vector[i]));
	}
	add(vector) {
		return this.concat(vector);
	}
	sub(vector) {
		return this.concat(vector, sub);
	}
	mul(vector) {
		return this.concat(vector, mul);
	}
	div(vector) {
		return this.concat(vector, div);
	}
	scale(factor) {
		return this.map(scale(factor));
	}
	abs() {
		return this.map(Math.abs);
	}
	mag() {
		return Math.sqrt(this.map(sqr).reduce(add));
	}
	dist(vector) {
		return mag(this.sub(vector));
	}
	equals(vector) {
		return this.every((x, i) => x === vector[i]);
	}
}

const fit = (a, b, greedy = true, round = greedy ? Math.ceil : Math.floor) => round(a / b);
const fit_and_wrap = (a, b, greedy = true) => fit(a, b, greedy) + 2 * b;

class List extends Array {
	constructor(...args) {
		super(...args);
	}
	fill(filler = i => undefined) {
		for (let i = 0; i < this.length; i++)
			this[i] = filler(i);
		return this;
	}
}

// Right, so we need to be able to manipulate this on the fly...
// We should be able to set the new dimensions, and the insert_row
// and column stuff should happen automagically....
class Grid extends List {
	get dimensions() {
		return vector(this.cols, this.rows);
	}

	constructor(rows = 0, cols = 0) {
		super(rows).fill(i => new List(cols).fill());
		values(this, {rows, cols});
	}

	// We use this to iterate for us... make things a touch easier....
	iterate(itr = (row, col) => console.log(`row: ${row}, col: ${col}`)) {
		this.forEach(row => row.forEach(col => itr(row, col)));
		return this;
	}

	// Re-think the anchor bizzz.
	update(rows, cols, anchor = Grid.BOTTOM_RIGHT,
						dims = this.dimensions,
						inds = anchor.mul(dims.sub(vector(1, 1))),
						diff = vector(cols, rows).sub(dims)) {
		// Not quite done... but getting there, yo.
		if (diff.y > 0)
			this.insert_rows(diff.y, inds.y);
		else (diff.y < 0)
			this.delete_rows(diff.y * -1, inds.y);
		if (diff.x > 0)
			this.insert_cols(diff.x, inds.x);
		else (diff.x < 0)
			this.delete_cols(diff.x * -1, inds.x)
	}

	insert_row(index = 0) {
		this.splice(index, 0, new List(this.cols).fill());
		this.rows++;
		return this;
	}
	insert_rows(quantity = 1, index = 0, _count = 0) {
		while (_count++ < quantity) // The increment happens AFTER the statement
			this.insert_row(index)
	}

	delete_row(index = this.rows - 1) {
		this.splice(index, 1);
		this.rows--;
		return this;
	}
	delete_rows(quantity = 1, index = this.rows - 1, _count = 0) {
		while(_count++ < quantity)
			this.delete_row(index);
	}

	insert_col(index = 0) {
		this.forEach(row => row.splice(index, 0, undefined));
		this.cols++;
		return this;
	}
	insert_cols(quantity = 1, index = 0, _count = 0) {
		while(_count++ < quantity)
			this.insert_col(index);
	}

	delete_col(index = this.cols - 1) {
		this.forEach(row => row.splice(index, 1));
		this.cols--;
		return this;
	}
	delete_cols(quantity = 1, index = this.cols - 1, _count = 0) {
		while(_count++ < quantity)
			this.delete_col(index);
	}
}
Grid.TOP_LEFT = vector(0, 0);
Grid.TOP_RIGHT = vector(1, 0);
Grid.BOTTOM_RIGHT = vector(1, 1);
Grid.BOTTOM_LEFT = vector(0, 1);

class TileController {
	cell = vector(10, 10);
	view = vector(100, 100);
	grid = vector(120, 120);

	_offset = vector(0, 0);
	_tile = null;
	_view = null;
	_grid = new Grid(12, 12);

	get offset() {
		return this._offset;
	}

	set offset(offset) {
		let cell = this.cell;
		let abs_offset = offset.abs();
		let diff = abs_offset.sub(cell);
		if (diff.y >= 0) {

			// We have vertical movement out-of-bounds!
			// we need to insert a new row and delete another
			// one based on the direction of our offset....

			// What does it mean when we "insert a row". It means
			// our offset is rejigged to reflect that we're kosher....
			// any GridItems that we're keeping track of, have their positions
			// updated.......... but how??
		}
		if (diff.x >= 0) {
			// We have horizontal movement out-of-bounds!
			// This means we need to go through each row and
			// insert a new column and delete another one
			// based on the direction of our offset.
		}
	}

	get rect() {

	}

	constructor(view, generate) {
		this._view = view.add(this._tile = generate());
		this._grid = new Grid();
		this.init(view)
	}

	init(view = this._view, tile = this._tile) {
		this.cell = vector(tile.rect.width, tile.rect.height);
		this.view = vector(view.rect.width, view.rect.height);
		this.grid = this.view.concat(this.cell, fit_and_wrap);
	}

	coords(row, col) {
		return vector(col, row).mul(this.cell);
	}
}
const observer = callback => new ResizeObserver(events => events.forEach(event => callback(event.target, event.contentRect)));

const event = (name, data = {}, bubbles = true, cancelable = true) =>
	Object.assign(new Event(name, {
		bubbles,
		cancelable
	}), data);
const defined = value => typeof value !== "undefined";
const resizer = observer((element, rect) => element.resizeCallback(rect));
const define = (prototype, _base = HTMLElement) => {
	class Definition extends _base {

		[CONNECTED] = false;
		[RECTANGLE] = new DOMRect();
		[LISTENERS] = [["@", x => x]];


		get parent() {
			return this.parentNode;
		}

		get rect() {
			return this[RECTANGLE];
		}

		constructor() {
			super();
			this[LISTENERS] = Object.entries(prototype)
				.filter(([key]) => key.startsWith(AT))
				.map(([key, listener]) => [key.substr(AT.length), listener]);
		}

		log(msg = '', logger = console.log) {
			logger(`${this.tagName} ${this.id ? '#' + this.id : ''} : ${this.className} => ${msg}`);
		}
		error(msg) {
			this.log(`ERROR: ${msg}`, console.error);
		}
		warn(msg) {
			this.log(`WARNING: ${msg}`, console.warn);
		}


		attr(key, value = undefined) {
			return defined(value) ? this.setAttribute(key, value) || value : this.getAttribute(key);
		}
		var(prop, value = undefined, key = `--${prop}`) {
			return defined(value) ? this.style.setProperty(key, value) : this.style.getPropertyValue(key);
		}


		add(el) {
			this.appendChild(el);
			return this;
		}
		remove(el) {
			this.removeChild(el);
			return this;
		}
		delete() {
			this.parent.remove(this);
		}
		copy() {
			return create(this.tagName, Array.from(this.attributes).reduce((obj, {name, value}) => (obj[name] = value && false) || obj, {}))
		}


		attach(channel, listener, captures = false) {
			channel === RESIZE && resizer.observe(this);
			this.addEventListener(channel, listener, captures);
			return this;
		}
		detach(channel, listener, captures = false) {
			channel === RESIZE && resizer.unobserve(this);
			this.removeEventListener(channel, listener, captures);
			return this;
		}
		dispatch(channel, data = {}, bubbles = true, cancelable = true) {
			this.dispatchEvent(event(channel, data, bubbles, cancelable));
			return this;
		}


		connectedCallback() {
			this[CONNECTED] = true;
			this[LISTENERS].forEach(([key, listener]) =>
										this.attach(key, listener));
			this.connected && this.connected();
			this.resizeCallback();
			this.renderCallback();
		}

		disconnectedCallback() {
			this[CONNECTED] = false;
			this[LISTENERS].forEach(([key, listener]) =>
										this.detach(key, listener));
			this.disconnected && this.disconnected();
		}

		resizeCallback(rect = this.getBoundingClientRect()) {
			this[RECTANGLE] = rect;
			this.dispatch(RESIZE, {rect});
		}

		renderCallback(ts = 0) {
			if (!this[CONNECTED] || !this.render)
				return;
			this.render(ts);
			requestAnimationFrame(ts => this.renderCallback.call(this, ts));
		}
	};
	Object.assign(Definition.prototype, prototype);
	return Definition;
}
const register = (tag, prototype, _base) => customElements.define(tag, define(prototype, _base));
const create = (tag, attributes = {}, _el = document.createElement(tag)) =>
	Object.entries(attributes).forEach(([key, value]) => _el.setAttribute(key, value)) || _el;

register("sc-tiler", {
	connected() {
		this.log('THIS IS CONNECTED, NIGGA.');

		const src = this.attr('tile-src');
		this.grid = new TileController(this, (x, y) => create("sc-tile", {
			x: px(x),
			y: px(y),
			src
		}));
	},
	"@transitionstart": function(e) {

	},
	"@transitionend": function(e) {
		console.log('Hello, world.');
	},
	"@resize": function(e) {
		// -->
		this.log('RENDERRRRINGGGG');
	},
	render(ts) {
		//this.log(`TILER RENDER @ ${ts}`);
	}
});

register("sc-tile", {
	connected() {
		this.add(create("img", {
			src: this.attr("src"),
		}));
	},
	get() {
		return vector(
			int(this.attr('x')),
			int(this.attr('y'))
		);
	},
	set(xy) {
		this.attr('x', px(xy.x));
		this.attr('y', px(xy.y));
	},
	move(offset) {
		this.set(this.get().add(offset));
	},
	render(ts) {
		this.var('x', this.attr('x'));
		this.var('y', this.attr('y'));
	}
});

register("cool-element", {
	connected() {
		this.log(`The text attribute says "${this.attr('text')}"`);
	},
	render(ts) {
		// this.log(ts);
	}
})

/*
__() {
		let tile;
		let width = 0;
		let height = 0;
		let h_offset = parseInt(this.attr("h-offset")) || 0;
		let offset_ratio = h_offset / 100;

		let h_padding = 10;
		let v_padding = 10;

		let row = 0;
		let col = 0;
		while (height < bounds.x) {
			while(width < bounds.y) {
				tile = create("sc-tile", init(
					(row % 2 ? h_offset : 0) + h_padding + width,
					v_padding + height
				));
				this.add(tile);
				width += tile.offsetWidth + h_padding;
				col++;
			}
			col = 0;
			width = 0;
			row++;
			height += tile.offsetHeight + v_padding;
		}
	},
*/
