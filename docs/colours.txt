Skycrate Brand Colours
-------------------------------

Colour ------------------------

Pink        -  #EE3078
Blue        -  #61C5BA
Orange      -  #F99D24

Grey --------------------------

Light       -  #F9F9F9
Tone 1      -  #F5F5F5
Tone 2      -  #E0E0E0

Midtone     -  #969696

Tone 4      -  #303030
Tone 5      -  #1B1B1B
Dark        -  #111111

Skycrate Web Colours
-------------------------------

Validation --------------------

Blue        -  #509ec9
Red         -  #CE2C2C
Amber       -  #E58220
Green       -  #6CC638